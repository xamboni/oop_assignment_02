

/*---------------------------------------------------------------------
 *
 * Date.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Date class implementation
 * 
 *
 ---------------------------------------------------------------------*/

#include "Date.h"
 
/*------------------------- Implementation ---------------------------*/

/* Date constructor. */
Date::Date(unsigned int initialYear, unsigned int initialMonth, unsigned int initialDay)
{
    Year = initialYear;
    Month = initialMonth;
    Day = initialDay;
}

/* Copy constructor */
Date::Date(const Date& copy_from)
{
    Year = copy_from.Year;
    Month = copy_from.Month;
    Day = copy_from.Day;
}

/* Copy assignment */
Date& Date::operator=(const Date& copy_from)
{
    Year = copy_from.Year;
    Month = copy_from.Month;
    Day = copy_from.Day;
}

/* Destructor */
Date::~Date()
{
    // Only primitives. No need to delete.
}

/* Date Getters */
unsigned int Date::GetYear() const
{
    return Year;
}

unsigned int Date::GetMonth() const
{
    return Month;
}

unsigned int Date::GetDay() const
{
    return Day;
}

std::string Date::to_string() const
{
    std::ostringstream ss;
    ss << std::setfill('0') << std::setw(2) << std::to_string(Month) << "/";
    ss << std::setfill('0') << std::setw(2) << std::to_string(Day) << "/";
    ss << std::setfill('0') << std::setw(4) << std::to_string(Year);
    std::string date_string(ss.str());
    return date_string;
}

/* Date Setters */
void Date::SetYear(unsigned int year)
{
    Year = year;
}

void Date::SetMonth(unsigned int month)
{
    Month = month;
}

void Date::SetDay(unsigned int day)
{
    Day = day;
}

