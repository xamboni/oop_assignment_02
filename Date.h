
/*---------------------------------------------------------------------
 *
 * Date.h
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Date class declaration
 * 
 *
 ---------------------------------------------------------------------*/
 
#ifndef DATE_H
#define DATE_H

#include <string>
#include <iomanip>
#include <iostream>
#include <sstream>

/*------------------------- Implementation ---------------------------*/
 
class Date
{

  public:
    Date(unsigned int initialYear, unsigned int initialMonth, unsigned int initialDay);
    Date(const Date& copy_from);
    Date& operator=(const Date& copy_from);
    ~Date();

    unsigned int GetYear() const;
    unsigned int GetMonth() const;
    unsigned int GetDay() const;
    std::string to_string() const;

    void SetYear(unsigned int year);
    void SetMonth(unsigned int year);
    void SetDay(unsigned int year);

 private:
    unsigned int Year;
    unsigned int Month;
    unsigned int Day;
};

#endif

