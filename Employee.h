
/*---------------------------------------------------------------------
 *
 * Employee.h
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Employee class declaration
 * 
 *
 ---------------------------------------------------------------------*/

#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>
#include "Date.h"
#include <sstream>

/*------------------------- Implementation ---------------------------*/
 
class Employee
{

  public:
    Employee(int employeeNumber, std::string employeeName, Date* hireDate);
    Employee(const Employee& copy_from);
    Employee& operator=(const Employee& copy_from);
    ~Employee();

    unsigned int GetEmployeeNumber() const;
    std::string GetEmployeeName() const;
    Date* GetHireDate() const;
    std::string to_string() const;
    
    void SetEmploymeeNumber(unsigned int employeeNumber);
    void SetEmployeeName(std::string employeeName);
    void SetHireDate(Date* hireDate);

 private:
    unsigned int EmployeeNumber;
    std::string EmployeeName;
    Date* HireDate;
};

#endif

