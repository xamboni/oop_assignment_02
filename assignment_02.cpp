
/*---------------------------------------------------------------------
 *
 * assignment_02.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Introduction to Classes
 * 
 * NOTE: Compile: `g++ -std=c++11 -o assignment_02 assignment_02.cpp Date.cpp Employee.cpp`
 *
 ---------------------------------------------------------------------*/
 
/*---------------------------- Includes ------------------------------*/
 
/* Standard */
#include <climits>
#include <cstdint>
#include <iostream>
#include <iomanip>
#include <time.h>
#include <ctime>
#include <sys/time.h>

/* User */
#include "Date.h"
#include "Employee.h"
 
/*----------------------------- Globals ------------------------------*/

/*------------------------------ Types -------------------------------*/
 
/*---------------------------- Constants -----------------------------*/

/*--------------------------- Prototypes -----------------------------*/

void print_time_difference(const char* action, timespec start_s, timespec stop_s);

/*------------------------- Implementation ---------------------------*/

/*
 * Inputs: 
 * Outputs: 
 * Note: 
 * 
----------------------------------------------------------------------*/
int main (int argc, char* argv[])
{
    timespec start_s;
    timespec stop_s;
    
    unsigned int test_variable = 0;
 
    uint32_t result = false;
    uint32_t random_employee_number;

    Date* sample_date_0;
    Date* sample_date_1;
    Date* sample_date_2;

    Employee* sample_employee_0;
    Employee* sample_employee_1;
    Employee* sample_employee_2;

    srand (time(NULL));

    sample_date_0 = new Date(2016, 12, 31);
    std::cout << sample_date_0->to_string() << std::endl;

    sample_date_1 = new Date(2015, 1, 1);
    std::cout << sample_date_1->to_string() << std::endl;

    sample_date_2 = new Date(22, 02, 02);
    std::cout << sample_date_2->to_string() << std::endl;

    random_employee_number = rand() % UINT_MAX;
    sample_employee_0 = new Employee(random_employee_number, std::string("Robert"), sample_date_0);
    std::cout << sample_employee_0->to_string() << std::endl;

    random_employee_number = rand() % UINT_MAX;
    sample_employee_1 = new Employee(random_employee_number, std::string("James"), sample_date_1);
    std::cout << sample_employee_1->to_string() << std::endl;

    random_employee_number = rand() % UINT_MAX;
    sample_employee_2 = new Employee(random_employee_number, std::string("McKenzie"), sample_date_2);
    std::cout << sample_employee_2->to_string() << std::endl;

    // Comparing execution times.
    std::cout << "--------------" << std::endl;

    // Comparing ++ vs x = x + 1 vs x += 1
    test_variable = 0;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable++;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("++: ", start_s, stop_s);

    test_variable = 0;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable = test_variable + 1;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("x = x + 1: ", start_s, stop_s);

    test_variable = 0;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable += 1;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("+=: ", start_s, stop_s);

    std::cout << "--------------" << std::endl;

    test_variable = 1;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable--;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("--: ", start_s, stop_s);

    test_variable = 1;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable = test_variable - 1;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("x = x - 1: ", start_s, stop_s);

    std::cout << "--------------" << std::endl;

    test_variable = 100;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable /= 10;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("/=: ", start_s, stop_s);

    test_variable = 100;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable = test_variable / 10;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("x = x / 10: ", start_s, stop_s);

    std::cout << "--------------" << std::endl;

    test_variable = 100;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable *= 10;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("*=: ", start_s, stop_s);

    test_variable = 100;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable = test_variable * 10;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("x = x * 10: ", start_s, stop_s);

    std::cout << "--------------" << std::endl;

    test_variable = 100;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable %= 10;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("%=: ", start_s, stop_s);

    test_variable = 100;
    clock_gettime(CLOCK_REALTIME, &start_s);
    test_variable = test_variable % 10;
    clock_gettime(CLOCK_REALTIME, &stop_s);
    print_time_difference("x = x % 10: ", start_s, stop_s);

    return result;
}

/*-------------------------------------------------------------------*/

void print_time_difference(const char* action, timespec start_s, timespec stop_s)
{
    uint64_t start = (uint64_t)start_s.tv_sec * 1000000LL + (uint64_t)start_s.tv_nsec / 1000LL;
    uint64_t stop = (uint64_t)stop_s.tv_sec * 1000000LL + (uint64_t)stop_s.tv_nsec / 1000LL;
    std::cout << "Cycle difference for " << action << stop - start << std::endl;
}

