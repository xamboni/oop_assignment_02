
/*---------------------------------------------------------------------
 *
 * Employee.cpp
 * 
 * 
 * Alexander Morgan
 * amorga36
 * Object Oriented Programming Spring 2017
 * Employee class implementation
 * 
 *
 ---------------------------------------------------------------------*/

#include "Employee.h"
 
/*------------------------- Implementation ---------------------------*/

/* Employee constructor. */
Employee::Employee(int employeeNumber, std::string employeeName, Date* hireDate)
{
    EmployeeNumber = employeeNumber;
    EmployeeName = std::string(employeeName);
    HireDate = hireDate;
}

/* Copy constructor */
Employee::Employee(const Employee& copy_from)
{
    Date* this_date = copy_from.HireDate;

    EmployeeNumber = copy_from.EmployeeNumber;
    EmployeeName = std::string(copy_from.EmployeeName);
    HireDate = new Date(*this_date);
}

/* Copy assignment */
Employee& Employee::operator=(const Employee& copy_from)
{
    EmployeeNumber = copy_from.EmployeeNumber;
    EmployeeName = copy_from.EmployeeName;
    HireDate = copy_from.HireDate;
}

/* Destructor */
Employee::~Employee()
{
    // Delete name from heap.
    delete(HireDate);
}

std::string Employee::to_string() const
{
    std::ostringstream ss;
    ss << "Employee Number: " << std::to_string(EmployeeNumber) << std::endl;
    ss << "Employee Name: " << EmployeeName << std::endl;
    ss << "Hire Date: " << HireDate->to_string();
    std::string employee_info_string(ss.str());
    return employee_info_string;
}

/* Employee Getters */
unsigned int Employee::GetEmployeeNumber() const
{
    return EmployeeNumber;
}

std::string Employee::GetEmployeeName() const
{
    return EmployeeName;
}

Date* Employee::GetHireDate() const
{
    return HireDate;
}

/* Employee Setters */
void Employee::SetEmploymeeNumber(unsigned int employeeNumber)
{
    EmployeeNumber = employeeNumber;
}

void Employee::SetEmployeeName(std::string employeeName)
{
    EmployeeName = employeeName;
}

void Employee::SetHireDate(Date* hireDate)
{
    HireDate = hireDate;
}

